1. Sejam os alfabetos E1= {a, b}, E2= {a, b, c, ... ,z } e E3= {0,1, 2, ... , 9}, e as linguagens sobre  este  alfabeto,  indicadas  a  seguir.  ````
Desenvolver  os  diagramas   de  estados  que reconheçam tais linguagens:
    * a.L1= { w e E1* | w começa com ae termina com b}
    * b.L2= { w e E1* | w possui aaa como subcadeia}
    * c.L3= { w e E1* | w possui baba como prefixo e abab como sufixo }
    * d.L4= { w e E1* | w possui no máximo uma ocorrência da cadeia baba}
    * e.L5= { w e E1* | w não possui ocorrência da cadeia baba}
    * f.L6= { w e E1* | w possui ocorrência par de a's seguida por ocorrência impar de b's} 
    * g.L7= { w e E2* | w começa com a e possui ocorrência par de a's ou começa com b e possui ocorrência impar de b's }
    * h.L8= { w e E3* | w inicia-se com 0e a soma de todos os seus dígitosé par, ou inicia-se com 1e a soma de todos os seus dígitos é impar}
    * i.L9= { w e (E2 U E3)* | w inicia-se com uma letra, possuindo a seguir qualquer combinação de letras e dígitos}
    * j.L10= { w e E3* U {+,-, e, ‘,’}| w é um número inteiro, um número decimal ou um número representado em notação científica}



2. A  partir  dos  diagramas  de  estado  definidos  no  item  anterior,  implemente  programas  de computador que simulem a ação destes diagramas. Tais   implementações   deverão   ser   feitas individualmenteou em trioutilizando   a linguagem de programação definida em sala de aula pelo professor.A  implementação  de  cada programadeverá  ser  salva  em  uma  pasta  identificando  o número da questão (por exemplo, a solução da questão adeveráser salva na pasta a). Para entregar, compacte  todas as  pastas(utilizando  o  Winrar  ou  outro  software  compactador) criando  um único arquivo  compactado  cujo  nome  deverá  ser  formado  pela  junção  do primeiro nome de cada elemento da dupla (por exemplo: Pedro e Roberto). A seguir envie o  arquivo  compactado  para  o  e-mail xxxxxxxxxxxxxxxxxx,  com  o  assunto: LFA -Implementação de Autômatos Finitos. No corpo do e-mail, especifique os nomes completos  de  cada  elemento  da  dupla. Importante: Certifique-se  de  excluir  qualquer arquivo executável do arquivo gerado, caso contrário o firewall poderá bloquear o seu e-mail.

    *  A data de entrega das implementações é 01/09/2019.  
